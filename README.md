# Билеты по методам оптимизации, матстату и матлингвистике

### Для использования репозитория:
1. Установить VSCode.
2. Установить *Markdown Preview Enhanced* от *Yiyi Wang*.
3. Залезть в *Settings* (*Ctrl+,*) -> *Extensions* -> *Markdown Preview Enhanced* и установить:
    - Math Rendering Option: Katex
    - Math Inline Delimiters: отредачить файл справа, добавив строку: <code>"markdown-preview-enhanced.mathInlineDelimiters": [["$`","`$"]]</code>
    - Mermaid Theme: менять настройку, пока не начнут отображаться дуги на графах.  
4. Перезагрузить VSCode.

### Для конвертирования в pdf:
1. Открыть превью .md файла. Возможные варианты:
    - Правой кнопкой на текст .md файла, затем *Markdown Preview Enhanced: Open Preview*.
    - Сверху справа есть иконки. Одна из них (с серой лупой) нужное.
    - Комбинация *Ctrl+K V*.
2. Тыкнуть правой кнопкой по тексту превью. Вылезет окошко с возможными вариантами конвертирования и другими опциями. 

### Различные руководства
* Справка по [markdown](https://paulradzkov.com/2014/markdown_cheatsheet/).
* Справка по [katex](https://katex.org/docs/supported.html).
* Справка по [mermaid](https://mermaidjs.github.io/flowchart.html). Пример в matlinquistica/tasks/t4.md.

Gitlab отображает markdown в стиле `gitlab`, рендерит математику с помощью $`\KaTeX`$, поэтому есть ряд отличий:
1. Строчные формулы вставляются не с помощью знаков <code>$$</code>, а вот такой конструкцией: $`y=x^2`$
2. Большие формулы оформляются, как будто это код:
```math
F = \frac
    {\alpha X}
    {\beta Y + \gamma Z}
```