## 26. СУ-схемы. Перевод, определяемый СУ-схемой.

Цепочка - последовательность лексем (терминалов)

Структура цепочки - способ задания семантики правильной последовательности лексем. Структура может выражать непосредственно семантические связи слов (для естественного языка) или, например, порядок вычислений (для арифметических выражений), вложенность конструкций (для языков программирования).

**Определение.** СУ-схема $`T=<V_{in}, V_{out}, V_N, I,R>`$, где
- $`V_{in}`$ - входной алфавит
- $`V_{out}`$ - выходной алфавит
- Правила в $`R`$ имеют вид $`A \to \varphi_1,\varphi_2`$, где
    - $`\varphi_1 \in (V_{in} \cup V_N)^*`$
    - $`\varphi_2 \in (V_{out} \cup V_N)^*`$
    - Количество и состав нетерминалов в $`\varphi_1`$ и $`\varphi_2`$ совпадают

СУ-схема называется простой, если порядок вхождения нетерминалов в части правил совпадают.

Применение СУ-схемы к цепочке состоит в том, чтобы из пары начальных символов по правилам схемы построить вывод таким образом, чтобы до запятой получилась исходная цепочка, а после - её перевод.

Формально можно записать так. Перевод, порождаемый СУ-схемой $`T`$:
```math
\tau(T)=\{ <x,y> \mid <I,I> \space \implies^+ \space <x,y> \space \& \space x \in V_{in}^*, y \in V_{out}^*\}
```