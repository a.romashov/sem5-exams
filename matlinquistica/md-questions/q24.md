## 24. Эквивалентные преобразования грамматик. Устранение цепных правил. Пример

Есть следующие виды преобразований:
* Устранение непроизводящих нетерминалов
* Устранение недостижимых нетерминалов
* Устранение $`\lambda`$-правил
* Устранение цепных правил

Цепные – правила вида A -> B
1.	Построение для каждого нетерминала множества $`N_A=\{B|A\Rightarrow^+ B\}`$:  
    a.Начальное значение $`N_A^0=\{A\}`$;  
    b. $`N_A^{i+1}=N_A^i\cup \{C|B\rightarrow C\in R\space\&\space B\in N_A^i\}`$;  
    с.Построение продолжается, пока не получим $`N_A^{i+1}=N_A^i\text{. Тогда }N_A^{i+1}=N_A`$.
2.	Построение множества правил преобразованной грамматики:  
    Если $`B\rightarrow\alpha\in  R`$ и не является цепным правилом, то включить в R' все правила вида $`A\rightarrow\alpha`$, для всех A таких, что $`B\in N_A`$
