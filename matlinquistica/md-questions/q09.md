## 9. Детерминизация автоматов с $`\lambda`$-переходами. Пример  
  
**Теорема.** Классы языков, допускаемых детерминированными автоматами и автоматами с $`\lambda`$-переходами, совпадают.

$`\square`$ Пусть автомат $`A=<Q,V_T,q_0,F,K>`$ - автомат с $`\lambda`$-переходами. Построим соответствующий детерменированный автомат $`A'=<Q',V_T,q_0',F',K'>`$ такой, что $`L(A)=L(A')`$. Определим далее:
```math
F'(q,a) = \{p \mid (q, ax) \vdash^+ (p,x)\}
```
```math
K'=K \cup \{ q \mid (q,\lambda) \vdash^* (p,\lambda) \& p \in K\}
```
$`\blacksquare`$

**Пример.**
Пусть есть автомат с $`\lambda`$-правилами:
```mermaid
graph LR
    A((A))
    style A fill:#3f3,stroke:#000,stroke-width:4px
    B((B))
    style B fill:#fff,stroke:#000,stroke-width:4px
    C((C))
    style C fill:#fff,stroke:#000,stroke-width:4px
    D((D))
    style D fill:#fff,stroke:#000,stroke-width:4px
    E((E))
    style E fill:#fff,stroke:#000,stroke-width:4px
    F((F#))
    style F fill:#fff,stroke:#000,stroke-width:4px

    A--a-->B
    A--lambda-->C
    B--b-->D
    C--a-->C
    C--lambda-->D
    D--a-->D
    D--b-->E
    E--c-->D
    D--a-->F
```

Построим функцию переходов детерметированного автомата:

||A|B|C|D|E|F|
|---|---|---|---|---|---|---|
|a|B|-|C|D,F|-|-|
|b|-|D|-|E|-|-|
|c|-|-|-|-|D|-|
|$`\lambda`$|C|-|D|-|-|-|

||A|[B,C,D,F]|[E]|[C,D,F]|[D,E]|[D]|[D,F]|
|---|---|---|---|---|---|---|---|
|a|[B,C,D,F]|[C,D,F]|-|[C,D,F]|[D,F]|[D,F]|[D,F]|
|b|[E]|[D,E]|-|[E]|[E]|[E]|[E]|
|c|-|-|[D]|-|[D]|-|-|

Конечные вершины - это [B,C,D,F], [D,F], [C,D,F].